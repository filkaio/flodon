# FLODON



## Getting started

The idea behind this project is to create simple modular pot, easy to build and 3d printed by anyone.
It should have modules like:
- soil moisture sensor
- temp&humidity sensor
- light sensor module
- pressure sensor module
- watering module
- cam module to detect the health of the plant
- external light

  Sensor modules should be modular as well, as actuators, to be easy to be added to the PCB. PCB should be as simple as possible, to be easy prepareable using simple technologies

  # Licence: 
  
  CC BY-SA 
